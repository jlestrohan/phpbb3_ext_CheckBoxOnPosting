<?php
/**
 *
 * Checkbox on Posting. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\checkposting\acp;

/**
 * Checkbox on Posting ACP module info.
 */
class main_info
{
	public function module()
	{
		return array(
			'filename'	=> '\jles\checkposting\acp\main_module',
			'title'		=> 'ACP_CHECKPOSTING_TITLE',
			'modes'		=> array(
				'settings'	=> array(
					'title'	=> 'ACP_CHECKPOSTING',
					'auth'	=> 'ext_jles/checkposting && acl_a_board',
					'cat'	=> array('ACP_CHECKPOSTING_TITLE')
				),
			),
		);
	}
}
