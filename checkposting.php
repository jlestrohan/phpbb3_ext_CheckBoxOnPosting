<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 20/04/2018
 * Time: 11:59
 */

namespace jles\checkposting;

/**
 * Core checkbos on post functionality used throughout the extension.
 *
 * @package jles/checkposting
 */

class checkposting
{
	const POSTS_CHECKBOX_NO = 1;
	const POSTS_CHECKBOX_YES = 2;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	public function __construct(\phpbb\db\driver\driver_interface $db)
	{
		$this->db = $db;
	}

	// returns true if the forum has the checkbox flag enabled
	public function forum_can_checkbox($forum_id)
	{
		$sql = $this->table_prefix .'SELECT * FROM ' . FORUMS_TABLE . ' WHERE forum_id = '.$forum_id;
		$result = $this->db->sql_query($sql);
		$forum_result = $this->db->sql_fetchrow($result);

		$this->db->sql_freeresult($result);
		return $forum_result['forum_allow_checkbox'];
	}

}