<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 20/04/2018
 * Time: 11:38
 */

namespace jles\checkposting\event;

use jles\checkposting\checkposting;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class acp_forums implements EventSubscriberInterface
{
	/** @var \tierra\topicsolved\topicsolved */
	protected $checkposting;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request */
	protected $request;

	/**
	 * Constructor
	 *
	 * @param checkposting $checkposting
	 * @param \phpbb\template\template $template
	 * @param \phpbb\request\request   $request
	 */
	public function __construct(checkposting $checkposting, \phpbb\template\template $template, \phpbb\request\request $request)
	{
		$this->checkposting = $checkposting;
		$this->template = $template;
		$this->request = $request;
	}

	/**
	 * Assign functions defined in this class to event listeners in core.
	 *
	 * @return array
	 */
	static public function getSubscribedEvents()
	{
		return array(
			'core.acp_manage_forums_initialise_data'    => 'acp_manage_forums_initialise_data',
			'core.acp_manage_forums_display_form'		=> 'acp_manage_forums_display_form',
			'core.acp_manage_forums_request_data'       => 'acp_manage_forums_request_data',
		);
	}

	/**
	 * Initialize default checkbox activated settings for new forums.
	 *
	 * @param $event
	 */
	public function acp_manage_forums_initialise_data($event)
	{
		$forum_data = $event['forum_data'];

		if ($event['action'] == 'add' && !$event['update'])
		{
			// if 1 all posts in that forum will require a box to be checked
			$forum_data['forum_allow_checkbox']    = 1;
		}

		$event['forum_data'] = $forum_data;
	}

	/**
	 * Format checkbox activated settings for forum edit form.
	 *
	 * @param $event
	 */
	public function acp_manage_forums_display_form($event)
	{
		$forum_data = $event['forum_data'];
		$template_data = $event['template_data'];

		$template_data = array_merge($template_data, array(
			'S_FORUM_ALLOW_CHECKBOX' => $forum_data['forum_allow_checkbox'],
			'POSTS_CHECKBOX_YES' => checkposting::POSTS_CHECKBOX_YES,
		));

		$event['template_data'] = $template_data;
	}

	/**
	 * Process forum checkbox settings form request data.
	 *
	 * @param $event
	 */
	public function acp_manage_forums_request_data($event)
	{
		$forum_data = $event['forum_data'];

		$forum_data = array_merge($forum_data, array(
			'forum_allow_checkbox'     => $this->request->variable('forum_allow_checkbox', 1),
		));

		$event['forum_data'] = $forum_data;
	}
}