<?php
/**
 *
 * Check Box on Posting. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license       GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\checkposting\event;

use jles\checkposting\checkposting;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Check Box on Posting Event listener.
 */
class main_listener implements EventSubscriberInterface
{
	/** @var \tierra\topicsolved\topicsolved */
	protected $checkposting;

	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\template\template */
	protected $template;

	protected $is_checkbox_visible;

	public function __construct( checkposting $checkposting, \phpbb\request\request $request, \phpbb\template\template $template )
	{
		$this->checkposting = $checkposting;
		$this->request = $request;
		$this->template     = $template;
	}

	static public function getSubscribedEvents()
	{
		return array(
			'core.submit_post_end'                          => 'post_submit_initialise_data',
			'core.posting_modify_template_vars'             => 'post_edit_screen_initialize_vars',
			//'core.posting_modify_submit_post_before'	                    	=> 'post_add_shareholder_data',
			'core.posting_modify_submission_errors'         => 'topic_is_shareholer_add_to_post_data',
		);
	}

	public function post_submit_initialise_data( $data )
	{
		//function that returns true if forum id has the checkbox flag
		// $this->checkposting->forum_can_checkbox($data['data']['forum_id']);
	}

	public function post_edit_screen_initialize_vars( $event )
	{
		//die(var_dump($event['post_data']));

		$this->is_checkbox_visible = $this->checkposting->forum_can_checkbox( $event['forum_id'] );

		$this->template->assign_vars( array(
				'S_CHECKBOX_TITLE'      => 'Je suis actionnaire de cette valeur',
				'S_CHECKBOX_VISIBLE'    => $this->is_checkbox_visible,
				'S_POSTS_CHECKBOX_YES'  => checkposting::POSTS_CHECKBOX_YES,
				'S_POSTS_CHECKBOX_NO'   => checkposting::POSTS_CHECKBOX_NO,
				'S_IS_SHAREHOLDER'      => (int)$event['post_data']['poster_is_shareholder'],
			)
		);
	}

	public function post_add_shareholder_data($event)
	{
		//die(var_dump($event['post_data']));

		//$event['data'] = array_merge($event['data'], array(
		//	'poster_is_shareholder'	=> $event['post_data']['post_checkbox_shareholder'],
		//));
	}

	public function topic_is_shareholer_add_to_post_data($event)
	{
		$event['post_data'] = array_merge($event['post_data'], array(
			'poster_is_shareholder'	=> (int)$this->request->variable('post_checkbox_shareholder', '', true),
		));
	}
}
