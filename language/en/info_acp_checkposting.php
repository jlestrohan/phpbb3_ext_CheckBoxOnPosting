<?php
/**
 *
 * Check Box on Posting. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_CHECKPOSTING_TITLE'			=> 'Checkbox on Posting',
	'CHECKBOX_SETTINGS'                 => 'Checkbox on Posts Settings',
	'ALLOW_CHECKBOX'                    => 'Allow checkbox on posts & topics on this forum',
	'ALLOW_CHECKBOX_EXPLAIN'            => 'Require from topic/posts starters to check a customized box before validating their message.',
));
