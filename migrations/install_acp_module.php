<?php
/**
 *
 * Check Box on Posting. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\checkposting\migrations;

class install_acp_module extends \phpbb\db\migration\migration
{
	/*public function effectively_installed()
	{
		return isset($this->config['acme_demo_goodbye']);
	}*/

	static public function depends_on()
	{
		return array( '\phpbb\db\migration\data\v31x\v314' );
	}

	public function update_data()
	{
		return array(
			array(
				'module.add',
				array(
					'acp',
					'ACP_CAT_DOT_MODS',
					'ACP_CHECKPOSTING_TITLE'
				)
			),
			array(
				'module.add',
				array(
					'acp',
					'ACP_CHECKPOSTING_TITLE',
					array(
						'module_basename' => '\jles\checkposting\acp\main_module',
						'modes'           => array( 'settings' ),
					),
				)
			),
		);
	}

	public function update_schema()
	{
		return array(
			'add_columns' => array(
				$this->table_prefix . 'forums' => array(
					'forum_allow_checkbox' => array( 'TINT:1', 0 ),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns' => array(
				$this->table_prefix . 'forums' => array(
					'forum_allow_checkbox',
				),
			),
		);

	}
}
