/**
 * Created by jack on 21/04/2018.
 */
jQuery.event.special.touchstart = {
    setup: function( _, ns, handle ){
        if ( ns.includes("noPreventDefault") ) {
            this.addEventListener("touchstart", handle, { passive: false });
        } else {
            this.addEventListener("touchstart", handle, { passive: true });
        }
    }
};


$(document).ready(function () {

   if ($('#post_checkbox_shareholder').is(":not(:checked)"))
       $('.default-submit-action').hide();

    $( ".shareholder_radio" ).click(function() {
        if ($('.default-submit-action').is(':hidden'))
            $('.default-submit-action').fadeIn('slow');
    });

});